sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nnext/iq/Processing/model/Formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, Formatter, History, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Diagram", {
		oFormatter: Formatter,
		onInit: function() {
			var ctrl = this;

			ctrl.getOwnerComponent().getRouter().getRoute("diagramFolder").attachPatternMatched(ctrl._onRouteMatched, ctrl);
			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
		},
		showProcessing: function(oEvent) {
			var ctrl = this;
			var oObject = oEvent.getSource().getBindingContext("diagram");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			ctrl._oRouter.navTo("processing", {
				Identify: oItem.Key.Identify
			});
		},
		onBackPress: function(oEvent) {
			this._oRouter.navTo("folder");
		},
		_onRouteMatched: function(oEvent) {
			var ctrl = this;
			var sFolderGuid = oEvent.getParameter("arguments").FolderGuid;
			var promiseDiagram = $.ajax("/Flow7Api/api/diagram");
			var promiseFolder = $.ajax("/Flow7Api/api/dashboard/processing/folder");

			$.when(promiseDiagram, promiseFolder)
				.done(function(xhrDiagram, xhrFolder) {
					var arrDiagrams = [];
					var oFolder = [];
					
					//取得篩選過的表單夾
					oFolder = xhrDiagram[0].filter(function(data) {
						return data.FolderGuid === sFolderGuid;
					});

					//取出已篩選表單夾的對應表單
					$.each(xhrFolder[0], function(i, item) {
						$.each(oFolder[0].DiagramLists, function(j, enableItem) {
							if (item.Key.Identify == enableItem.Identify) {
								arrDiagrams.push(item);
								return false;
							}
						});
					});

					//將取出的表單放入model
					var oData = new sap.ui.model.json.JSONModel(arrDiagrams);
					ctrl.getView().setModel(oData, "diagram");
				});
		}
	});
});