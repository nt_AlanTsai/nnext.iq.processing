sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nnext/iq/Processing/model/Formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, Formatter, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Processing", {
		oFormatter: Formatter,
		onInit: function() {
			var ctrl = this;

			ctrl.getOwnerComponent().getRouter().getRoute("processing").attachPatternMatched(ctrl._onRouteMatched, ctrl);
			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);

			$.ajax("/Flow7Api/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "processing");
				});
		},
		_onRouteMatched: function(oEvent) {
			var ctrl = this;
			var sIdentify = oEvent.getParameter("arguments").Identify;

			// build filter array
			var aFilter = [];
			aFilter.push(new Filter("Identify", FilterOperator.Contains, sIdentify));

			// filter binding
			var oList = ctrl.getView().byId("tableProcessing");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		},
		showDetail: function(oEvent) {
			var ctrl = this;
			var oObject = oEvent.getSource().getBindingContext("processing");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			ctrl._oRouter.navTo("detail", {
				RequisitionId: oItem.RequisitionId
			});
		}
	});
});