sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Folder", {
		onInit: function() {
			var ctrl = this;

			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);

			$.ajax("/Flow7Api/api/diagram")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "folder");
				});
		},
		showDiagram: function(oEvent) {
			var ctrl = this;
			var oObject = oEvent.getSource().getBindingContext("folder");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			ctrl._oRouter.navTo("diagramFolder", {
				FolderGuid: oItem.FolderGuid
			});
		}
	});
});