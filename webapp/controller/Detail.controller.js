sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Detail", {
		onInit: function() {
			var ctrl = this;

			ctrl.getOwnerComponent().getRouter().getRoute("detail").attachPatternMatched(ctrl._onRouteMatched, ctrl);
			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
		},
		onBackPress: function(oEvent) {
			var ctrl = this;
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
				oRouter.navTo("overview", {}, true);
			}
		},
		_onRouteMatched: function(oEvent) {
			var ctrl = this;
			var sRequisitionId = oEvent.getParameter("arguments").RequisitionId;

			$.ajax("/Flow7Api/api/fdp/m/" + sRequisitionId)
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "detail");
				});
		}
	});
});