sap.ui.define(function() {
	"use strict";

	var Formatter = {
		processingCount: function(nCount) {
			if (nCount > 10) {
				return "Error";
			} else if (nCount > 5) {
				return "Warning";
			} else {
				return "Success";
			}
		},
		processApprover: function(fValue){
			var matches = fValue.match(/[^\]@\[]+/g);
			return matches[1];
		}
	};

	return Formatter;

}, /* bExport= */ true);